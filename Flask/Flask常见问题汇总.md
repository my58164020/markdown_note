# Flask常见问题汇总

## 导读

> 整合常见的Flask报错及解决方法。

### 1、连接Mysql时出现 Warning 1366

- 解决方法

```python
import mysql.connector

app.config["SQLALCHEMY_DATABASE_URI"] = "mysql+mysqlconnector://账号:密码@localhost/appname"

```

### 2、ModuleNotFoundError: No module named 'flask._compat'

- 解决方法

```python
# 修改flask_script/__init__.py 

# from ._compat import text_type 改为下面导入路径
from flask_script._compat import text_type

```
