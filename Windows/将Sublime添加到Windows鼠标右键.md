# 将Sublime添加到Windows鼠标右键

## 导读

> 非常实用的小技巧。

### 一、右键文件-管理员

1. win+R输入‘regedit’，打开注册表。

2. 找到‘HKEY_CLASSES_ROOT\*\shell’目录。

3. 新建项‘Open with Sublime’，更改右键文字显示内容‘Edit with Sublime’。

4. 新建字符串值‘Icon’，更改程序图标路径‘C:\PortableFiles\Sublime\Sublime.exe’。

5. 在‘Open with Sublime’项中新建子项‘command’，更改程序路径‘"C:\PortableFiles\Sublime\Sublime.exe" "%1"’。

### 二、右键文件夹空白区域-管理员

1. win+R输入‘regedit’，打开注册表。

2. 找到‘HKEY_CLASSES_ROOT\Directory\shell’。

3. 新建项‘Sublime’，更改右键文字显示内容‘Open Folder as Sublime Project’。

4. 新建字符串值‘Icon’，更改程序图标路径‘C:\PortableFiles\Sublime\Sublime.exe’。

5. 在‘Sublime’项中新建子项‘command’，更改程序路径‘"C:\PortableFiles\Sublime\Sublime.exe" "%1"’。

### 三、右键桌面空白区域-管理员

1. win+R输入‘regedit’，打开注册表。

2. 找到‘HKEY_CLASSES_ROOT\Directory\Background\shell’。

3. 新建项‘Sublime’，更改右键文字显示内容‘Open Folder as Sublime Project’。

4. 新建字符串值‘Icon’，更改程序图标路径‘C:\PortableFiles\Sublime\Sublime.exe’。

5. 在‘Sublime’项中新建子项‘command’，更改程序路径‘"C:\PortableFiles\Sublime\Sublime.exe" "%V"’。
