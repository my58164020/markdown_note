# Windows任务栏时间显示秒

## 导读

> 让Windows的任务栏时钟显示秒数。

1. win+R输入’regedit‘，打开注册表。

2. 找到’HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced‘目录。

3. 右键空白处，选择新建一个’DWORD（32位）值‘，将新建的项目命名为’ShowSecondsInSystemClock‘。

4. 双击打开’ShowSecondsInSystemClock‘，把数值数据设置为’1‘。

5. 重启电脑，任务栏时间显示就有秒了。
