# Git安装方法

## 导读

> Git在不同平台下的安装方法。

### 1、CentOS7

- 安装

```shell
yum install -y git
```

- 配置

```shell
git config --global user.name "guoxianru"
git config --global user.email "admin@addcoder.com"
```

- 创建公钥

```shell
ssh-keygen -C 'admin@addcoder.com' -t rsa
cd ~/.ssh
vim id_rsa.pub
```

- 安装新版本(root)

```shell
# 新建新yum存储库配置文件
vim /etc/yum.repos.d/wandisco-git.repo

# 插入以下内容并保存退出
[wandisco-git]
name=Wandisco GIT Repository
baseurl=http://opensource.wandisco.com/centos/7/git/$basearch/
enabled=1
gpgcheck=1
gpgkey=http://opensource.wandisco.com/RPM-GPG-KEY-WANdisco

# 导入存储库GPG密钥
rpm --import http://opensource.wandisco.com/RPM-GPG-KEY-WANdisco

# 安装
yum install git

# 验证
git --version
```

### 2、Ubuntu16.04

- 安装git

```shell
sudo apt-get install git
```
