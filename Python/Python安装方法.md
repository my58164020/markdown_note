# Python安装方法

## 导读

> Python在不同平台下的安装方法。

### 1、CentOS7

- 安装Python3.8.8

```shell
# 准备编译环境
sudo yum groupinstall -y 'Development Tools'
sudo yum install -y kernel-headers zlib-devel bzip2-devel openssl-devel ncurses-devel libffi-devel xz-devel sqlite-devel sqlite*

# 下载Python3.8.8压缩包
wget --no-check-certificate https://www.python.org/ftp/python/3.8.8/Python-3.8.8.tgz

# 创建安装目录
mkdir /usr/local/python38

# 解压
tar -zxvf Python-3.8.8.tgz

# 切换到解压后的根目录
cd Python-3.8.8/

# 编译安装
./configure --with-ssl --prefix=/usr/local/python38
make
make install
```

- 创建Python3链接

Linux里原来的python命令还是指向Python2，这里创建python3的软链接指向Python3，这样Python2和Python3就都可以用了。

```shell
# -snf 强制替换已有的链接
ln -snf /usr/local/python38/bin/python3 /usr/bin/python3
```

- 创建Pip3链接

保留pip指向Pip2，创建pip3的软链接指向Pip3。

```shell
# -snf 强制替换已有的链接
ln -snf /usr/local/python38/bin/pip3 /usr/bin/pip3
```

### 2、Ubuntu16.04

- 安装Python3.8

```shell
# 添加源仓库
sudo add-apt-repository ppa:jonathonf/python-3.8

# 先更新，再安装
sudo apt-get update
sudo apt-get install python3.8

# 安装pip
sudo apt-get install python-pip
sudo apt-get install python3-pip

# 升级pip
pip install --upgrade pip
```
