# MySQL远程连接

## 导读

> 远程连接服务器上的MySQL。

- 打开命令行

```shell
# 使用root登录
mysql -uroot -p密码
```

- 创建远程访问用户

```shell
create user '用户名'@'IP地址' identified by '密码';
# create user 'root'@'%' identified by '123456';
```

- 赋予用户最大权限

```shell
grant all privileges on *.* to '用户名'@'IP地址' identified by '密码';
# grant all privileges on *.* to 'root'@'%' identified by '123456';
```

- 刷新权限

```shell
flush privileges;
```

- 连接

```shell
mysql -h IP地址 -P 端口 -u远程访问用户 -p密码
```

- 连接不上可能出现的原因

```shell
ERROR 1396 (HY000): Operation CREATE USER failed for 'root'@'%'，说明已经存在该用户对应主机，可以直接赋予用户权限。

ERROR 1045 (28000): Access denied for user 'root'@'%' (using password: YES)，密码不对。

防火墙阻挡远程连接数据库，关闭防火墙或允许连接MySQL。
```
