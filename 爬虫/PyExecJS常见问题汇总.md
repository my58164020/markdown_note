# PyExecJS常见问题汇总

## 导读

> Python使用PyExecJS调用JS代码出现的错误，记录一下。

### 1、编码未指定

- 问题描述

```python
# UnicodeEncodeError: 'gbk' codec can't encode character '\ufffd' in position 160: illegal multibyte sequence
```

该异常出现的场景为在Windows系统下使用PyExecJS运行JS代码，JS代码中包含中文或其他特殊字符。

- 解决方法为

修改Python安装目录下lib文件夹里的subprocess.py文件
(C:\Users\Administrator\AppData\Local\Programs\Python\Python38\Lib\subprocess.py)
修改encoding=None为encoding="utf-8"

```python
def __init__(self, args, bufsize=-1, executable=None,
             stdin=None, stdout=None, stderr=None,
             preexec_fn=None, close_fds=True,
             shell=False, cwd=None, env=None, universal_newlines=None,
             startupinfo=None, creationflags=0,
             restore_signals=True, start_new_session=False,
             pass_fds=(), *, encoding="utf-8", errors=None, text=None):
    """Create new Popen instance."""
```
