# 部署sekiro并升级为HTTPS

## 导读

> sekiro是一个基于长链接和代码注入的API暴露框架，可以用在js/app逆向、web/app数据抓取等场景，本文主要介绍如何部署该框架和升级为https的方法。

### 一、版本选择及部署

[sekiro官方文档](https://sekiro.virjar.com/sekiro-doc/)

第一代sekiro从2019年开发，自2021年4月终止开发，由于早期代码有太多底层架构方面的问题。故2021年进行了彻底的重构，并且本次重构试一次完全不兼容的重构。

我们建议使用2020(含)以前的版本的同学，尽快升级到新版。同时sekiro也关闭了老版本的docker自动构建通道。

- Sekiro服务端（旧版），服务器开放端口：5600-5603

[旧版](https://github.com/virjar/sekiro/tree/a9b115709fbae88889a123e07fc09624d57ebe43)

```shell
# 安装docker
docker run --restart=always -d -p 5600:5600 -p 5601:5601 -p 5602:5602 -p 5603:5603 --name sekiro-server registry.cn-beijing.aliyuncs.com/virjar/sekiro-server:latest
```

- Sekiro服务端（新版），服务器开放端口：5620

[新版](https://github.com/virjar/sekiro)

[下载demo](https://oss.virjar.com/sekiro/sekiro-demo)

```shell
# 需要Java环境
nohup sh /bin/sekiro.sh
```

### 二、升级HTTPS

由于本人Java知识匮乏，所以使用Flask+uWSGI构建服务端，服务器需要有Java环境。

#### 0.启动sekiro

在项目根目录下创建文件：api_sekiro_run.sh

```shell
#!/bin/bash

ps -ef | grep sekiro-service-demo | grep -v grep | awk '{print $2}' | xargs kill -9

sleep 3

nohup sh /srv/api_sekiro/bin/sekiro.sh >>/srv/api_sekiro/logs/nohup_sekiro_server.log 2>&1 &
```

启动sekiro

```shell
cd /srv/api_sekiro && sh /srv/api_sekiro/api_sekiro_run.sh
```

#### 1.flask

在项目根目录下创建文件：manage.py

```python
import logging
import os

from flask import Flask, render_template
from flask_script import Manager, Server

logging.basicConfig(level=logging.INFO)
app = Flask(__name__, template_folder="templates")
manager = Manager(app)
manager.add_command("runserver", Server("0.0.0.0", port=11002))


def start_sekiro():
    os.popen("cd /srv/api_sekiro && sh /srv/api_sekiro/api_sekiro_run.sh")


def stop_sekiro():
    os.popen(
            "ps -ef | grep sekiro-service-demo | grep -v grep | awk '{print $2}' | xargs kill -9"
    )


@app.route("/", methods=["GET"], strict_slashes=False)
def index():
    return render_template("index.html")


@app.route("/start", methods=["GET"], strict_slashes=False)
def start():
    try:
        start_sekiro()
        return {"code": "1", "msg": "sekiro服务启动成功"}
    except:
        return {"code": "0", "msg": "sekiro服务启动失败"}


@app.route("/stop", methods=["GET"], strict_slashes=False)
def stop():
    try:
        stop_sekiro()
        return {"code": "1", "msg": "sekiro服务关闭成功"}
    except:
        return {"code": "0", "msg": "sekiro服务关闭失败"}


if __name__ == "__main__":
    manager.run()

```

在创建证书之前，在项目根目录下创建文件夹：/templates，在该文件夹下创建文件：index.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sekiro服务</title>
    <script src="/static/sekiro_web_client.js"></script>
    <link rel="icon" href="/static/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/static/favicon.ico" type="image/x-icon">
</head>
<body>

<h3>接口说明</h3>
<p>
    <span>
        1. /start
    </span><br><br>
    <span>
        启动sekiro服务
    </span><br><br>
    <span>
        2. /stop
    </span><br><br>
    <span>
        关闭sekiro服务
    </span><br><br>
    <span>
        3. /business-demo/groupList
    </span><br><br>
    <span>
        sekiro服务的连接列表
    </span><br><br>
    <span>
        4. wss://sekiro.xxx.com/business-demo/register?group=test&clientId=xxx
    </span><br><br>
    <span>
        客户端连接服务端
    </span><br><br>
    <span>
        5. https://sekiro.xxx.com/business-demo/invoke?group=test&action=xxx&xxx=xxx
    </span><br><br>
    <span>
        服务端调用客户端
    </span><br><br>
</p>

<h3>Sekiro服务部署</h3>
<p>
    <span>
        1. Sekiro服务端（新版），服务器开放端口：5620
    </span><br><br>
    <span>
        https://sekiro.virjar.com/sekiro-doc/02_server_deploy/1.DemoServer.html.com/virjar/sekiro-server:latest
    </span><br><br>
    <span>
        2. Sekiro服务端（旧版），服务器开放端口：5600-5603
    </span><br><br>
    <span>
        docker run --restart=always -d -p 5600:5600 -p 5601:5601 -p 5602:5602 -p 5603:5603 --name sekiro-server registry.cn-beijing.aliyuncs.com/virjar/sekiro-server:latest
    </span><br><br>
</p>

</body>
</html>
```

#### 2.uwsgi

- 配置uwsgi.ini文件

在项目文件夹与manage.py同级的目录下创建uwsgi.ini，文件内容如下（注意路径）：

```shell
[uwsgi]
# uwsgi监听的socket，一会儿配置Nginx会用到
socket = 127.0.0.1:11002
# 在app加载前切换到该目录，设置为Flask项目根目录
chdir = /srv/api_sekiro
# 加载指定的python WSGI模块，设置为Flask项目的manage文件
wsgi-file = ./manage.py
# 指定app对象实例
callable = app
# 启动一个master进程来管理其他进程
master = true
# 工作的进程数
processes = 2
# 每个进程下的线程数量
threads = 4
# 当服务器退出的时候自动删除unix socket文件和pid文件
vacuum = true
# 使进程在后台运行，并将日志打到指定的日志文件或者udp服务器
daemonize = /srv/api_sekiro/uwsgi.log

```

- 加载配置文件

```shell
uwsgi --ini uwsgi_api_sekiro.ini
# 出现getting INI configuration from uwsgi.ini（成功）
```

- 项目有更新的时候，需要先关闭uwsgi然后重启即可

- 基本命令

```shell
# 启动uwsgi服务器
uwsgi --ini uwsgi_api_sekiro.ini

# 查看uwsgi是否运行
ps -aux | grep uwsgi

# 查看端口号占用
netstat -anp | grep 5050

# 结束uwsgi进程
pgrep uwsgi | xargs kill -s 9
```

#### 3.Let’s Encrypt免费证书

在创建证书之前，在项目根目录下创建文件夹：/.well-know，在该文件夹下创建文件：acme-challenge

#### 4.配置Nginx

编辑配置文件

```shell
sudo vim /etc/nginx/conf.d/sekiro.conf
```

示例

```shell
# sekiro框架
upstream sekiro_business_netty {
  server 127.0.0.1:5620;
}

# 网站配置
server {
    # 设置监听端口
    listen 80;
    # 设置对外访问入口,可以是域名可以是公网IP
    server_name sekiro.xxx.com;
    # 设置虚拟主机的基本信息
    location / {
        include uwsgi_params;
        uwsgi_pass 127.0.0.1:11002;
        uwsgi_read_timeout 5;
    }
    # 静态文件设置
    location /static {
        expires 30d;
        alias /srv/api_sekiro/static/;
    }
    # 创建Let’s Encrypt免费SSL证书临时文件
    location /.well-known/acme-challenge {
        alias /srv/api_sekiro/.well-known/acme-challenge;
    }
    # sekiro框架
    location /business-demo {
        proxy_http_version 1.1;
        proxy_read_timeout 500;
        proxy_connect_timeout 300;
        proxy_redirect off;
        proxy_set_header Upgrade            $http_upgrade;
        proxy_set_header Host               $http_host;
        proxy_set_header X-Real-IP          $remote_addr;
        proxy_set_header X-Forwarded-For    $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto  $scheme;
        proxy_pass http://sekiro_business_netty;
    }
    # 允许网段
    allow all;
    # 设置访问的语言编码
    charset UTF-8;
    # nginx的超时参数设置为60秒
    send_timeout 60;
    # 实IP在X-Forwarded-For请求头中
    real_ip_header X-Real-IP;
    # X-Forwarded-For请求头中的最后一个IP当成真实IP
    real_ip_recursive off;
    # 设置fastcgi缓冲区为8块128k大小的空间
    fastcgi_buffers 8 128k;
    # 上传文件大小限制,默认1m
    client_max_body_size 0;
    # 访问日志记录
    access_log /var/log/nginx/sekiro_access.log;
    # 错误日志记录
    error_log /var/log/nginx/sekiro_error.log;
    # 开启gzip
    gzip on;
    # 是否在http header中添加Vary:Accept-Encoding
    gzip_vary on;
    # 设置压缩所需要的缓冲区大小
    gzip_buffers 32 4K;
    # gzip 压缩级别,1-9,数字越大压缩的越好,也越占用CPU时间
    gzip_comp_level 5;
    # 启用gzip压缩的最小文件,小于设置值的文件将不会压缩
    gzip_min_length 100;
    # 配置禁用gzip条件,支持正则
    gzip_disable "MSIE [1-6]\.";
    # 进行压缩的文件类型
    gzip_types application/javascript text/css text/xml;
    # HTTP严格传输安全的 max-age 需要大于15768000秒
    add_header Strict-Transport-Security "max-age=31536000";
    # 配置nginx404错误配置
    error_page 404  /404.html;
    location = /404.html {
        root /usr/share/nginx/html;
    }
    # 配置nginx502错误配置
    error_page 502  /502.html;
    location = /502.html {
        root /usr/share/nginx/html;
    }
}
```

#### 5.重启nginx，以便生效

```shell
# centos7
systemctl restart nginx

# Ubuntu16.04
/etc/init.d/nginx restart
```

#### 6.安装certbot

```shell
# centos7
yum install -y certbot

# Ubuntu16.04
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
sudo apt-get install certbot
```

#### 7.签发SSL证书

两种生成证书的方式

- andalone

certbot 会启动自带的 nginx（如果服务器上已经有nginx或apache运行，需要停止已有的nginx或apache）生成证书。

```shell
certbot certonly --standalone -d sekiro.xxx.com
```

- webroot(推荐)

```shell
certbot certonly --webroot -w /srv/api_sekiro -d sekiro.xxx.com
```

-w：指定项目绝对路径。

-d：指定生成证书域名，不可以直接写IP。

这条命令的输出类似于这样(Congratulations)为成功。

证书位置 /etc/letsencrypt/live/xxx.com/fullchain.pem

私钥位置 /etc/letsencrypt/live/xxx.com/privkey.pem

#### 8.生成前向安全性密钥

```shell
cd /etc/letsencrypt/live
openssl dhparam 2048 -out dhparam.pem
```

#### 9.将https配置进Nginx

- 配置

```shell
# sekiro框架
upstream sekiro_business_netty {
  server 127.0.0.1:5620;
}

# 网站配置
server {
    # 设置监听端口
    listen 80;
    # 设置监听端口,开启https,默认端口
    listen 443 ssl;
    # 设置对外访问入口,可以是域名可以是公网IP
    server_name sekiro.xxx.com;
    # HTTP请求301永久跳转到HTTPS
    if ($server_port = 80) {
        return 301 https://$server_name$request_uri;
    }
    # 设置虚拟主机的基本信息
    location / {
        include uwsgi_params;
        uwsgi_pass 127.0.0.1:11002;
        uwsgi_read_timeout 5;
    }
    # 静态文件设置
    location /static {
        expires 30d;
        alias /srv/api_sekiro/static/;
    }
    # 创建Let’s Encrypt免费SSL证书临时文件
    location /.well-known/acme-challenge {
        alias /srv/api_sekiro/.well-known/acme-challenge;
    }
    # sekiro框架
    location /business-demo {
        proxy_http_version 1.1;
        proxy_read_timeout 500;
        proxy_connect_timeout 300;
        proxy_redirect off;
        proxy_set_header Upgrade            $http_upgrade;
        proxy_set_header Host               $http_host;
        proxy_set_header X-Real-IP          $remote_addr;
        proxy_set_header X-Forwarded-For    $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto  $scheme;
        proxy_pass http://sekiro_business_netty;
    }
    # session会话的缓存类型和大小
    ssl_session_cache shared:SSL:10m;
    # ession会话的超时时间
    ssl_session_timeout 10m;
    # 依赖SSLv3和TLSv1协议的服务器密码将优先于客户端密码
    ssl_prefer_server_ciphers on;
    # 证书位置
    ssl_certificate /etc/letsencrypt/live/sekiro.xxx.com/fullchain.pem;
    # 私钥位置
    ssl_certificate_key /etc/letsencrypt/live/sekiro.xxx.com/privkey.pem;
    # 前向安全性,DH-Key交换密钥文件位置
    ssl_dhparam /etc/letsencrypt/live/dhparam.pem;
    # PCI DSS支付卡行业安全标准,禁用不安全的SSLv1 2 3,只使用TLS,PCI安全标准委员会规定开启TLS1.0将导致PCI DSS不合规
    ssl_protocols TLSv1.1 TLSv1.2;
    # 需要配置符合PFS规范的加密套件
    ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE:ECDH:AES:HIGH:!NULL:!aNULL:!MD5:!ADH:!RC4:!DH:!DHE;
    # 允许网段
    allow all;
    # 设置访问的语言编码
    charset UTF-8;
    # nginx的超时参数设置为60秒
    send_timeout 60;
    # 实IP在X-Forwarded-For请求头中
    real_ip_header X-Real-IP;
    # X-Forwarded-For请求头中的最后一个IP当成真实IP
    real_ip_recursive off;
    # 设置fastcgi缓冲区为8块128k大小的空间
    fastcgi_buffers 8 128k;
    # 上传文件大小限制,默认1m
    client_max_body_size 0;
    # 访问日志记录
    access_log /var/log/nginx/sekiro_access.log;
    # 错误日志记录
    error_log /var/log/nginx/sekiro_error.log;
    # 开启gzip
    gzip on;
    # 是否在http header中添加Vary:Accept-Encoding
    gzip_vary on;
    # 设置压缩所需要的缓冲区大小
    gzip_buffers 32 4K;
    # gzip 压缩级别,1-9,数字越大压缩的越好,也越占用CPU时间
    gzip_comp_level 5;
    # 启用gzip压缩的最小文件,小于设置值的文件将不会压缩
    gzip_min_length 100;
    # 配置禁用gzip条件,支持正则
    gzip_disable "MSIE [1-6]\.";
    # 进行压缩的文件类型
    gzip_types application/javascript text/css text/xml;
    # HTTP严格传输安全的 max-age 需要大于15768000秒
    add_header Strict-Transport-Security "max-age=31536000";
    # 配置nginx404错误配置
    error_page 404  /404.html;
    location = /404.html {
        root /usr/share/nginx/html;
    }
    # 配置nginx502错误配置
    error_page 502  /502.html;
    location = /502.html {
        root /usr/share/nginx/html;
    }
}
```

- 重启nginx，以便生效

```shell
# 切换到项目目录下运行
uwsgi --ini uwsgi_api_sekiro.ini

# 重启nginx服务
systemctl restart nginx
```
