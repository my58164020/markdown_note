# Requests设置“verify=False”时忽略Warning方法

## 导读

> 使用Requests时遇到的问题，记录下来。

- 问题描述

使用Python3的requests发送HTTPS请求，关闭认证（verify=False）情况下，控制台会输出警告。

```python
"""
InsecureRequestWarning: Unverified HTTPS request is being made.

Adding certificate verification is strongly advised.
"""
```

- 解决办法

在语句前加上以下代码即可不会输出警告。

```python
from urllib3 import disable_warnings
from urllib3.exceptions import InsecureRequestWarning

# 忽略HTTPS警告
disable_warnings(InsecureRequestWarning)

```
