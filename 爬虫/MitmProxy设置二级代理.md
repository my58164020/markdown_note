# MitmProxy设置二级代理

## 导读

> 通过python脚本自定义二级代理的代理方式。

### 1、应用场景

需要对设备进行监听抓包，且设备需要通过代理ip与外部进行通信。

### 2、基本原理

受监听设备通过mitmproxy代理，然后mitmproxt将设备的请求转发到与外部交互的代理(下面称为外层代理)，
从外部代理返回的响应也会经过mitmproxy再给到设备。相当于在设备与外部代理之间加上了一个mitmproxy代理进行监听。

### 3、使用方法

```python
# test.py
class TeestMitmProxy:
    def __init__(self):
        self.name = "mitmproxy"

    def request(self, flow):
        """处理经过mitmproxy的请求"""
        address = ("127.0.0.1", 7890)  # 这里输入的是外层代理
        if flow.live:
            flow.live.change_upstream_proxy_server(address)  # 这里将请求转发到了外部代理

    def response(self, flow):
        """处理经过mitmproxy的响应"""
        print(flow.request.url)


addons = [TeestMitmProxy()]
```

```shell
# 启动命令
mitmdump.exe --mode upstream:http://127.0.0.1:7890/ -s D:/test.py -p 3838
```
