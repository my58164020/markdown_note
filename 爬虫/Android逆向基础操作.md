# Android逆向基础操作

## 导读

> Android逆向基础操作，包含adb、frida、objection、抓包、脱壳等。

### 一、adb

#### 1.连接设备

```shell
# 所有连接设备
adb devices

# 远程连接设备
adb connect 192.168.137.11:5555

# 操作指定设备
adb -s 设备号 其他指令
# adb -s 127.0.0.1:21503 shell
# adb -s FA6AE0309067 shell
# 127.0.0.1:5555 蓝叠
# 127.0.0.1:7555 MUMU模拟器
# 127.0.0.1:62001 夜游神模拟器
# 127.0.0.1:21503 逍遥模拟器
```

- 模拟器adb连接不上解决方法：替换模拟器自带的adb

#### 2.操作设备

```shell
# 查看Android处理器架构
adb shell getprop ro.product.cpu.abi

# 安装APP
adb install xxx.apk
# 安装APP,覆盖安装
adb install -r xxx.apk

# 卸载APP
adb uninstall app包名
# 卸载APP,保留数据
adb uninstall -k app包名
# adb shell cmd package uninstall -k  com.tencent.mm

# 传文件，电脑->手机
adb push 电脑文件路径 手机路径
# adb push frida-server-14.2.17-android-arm64 /data/local/tmp

# 传文件，手机->电脑
adb pull 手机文件路径
# adb pull /system/build.prop

# 移动文件
adb shell
mv /sdcard/Download/frida-server-14.2.17 /data/local/tmp/

# 修改文件权限
adb shell
su
chmod 644 /system/build.prop
```

#### 3.操作APP

```shell
# 手机端安装的所有app包名
adb shell pm list packages

# 查看当前包名和主Activity
adb shell dumpsys window | findstr mCurrentFocus

# 启动APP
adb shell am start 包名/主Activity
# adb shell am start com.autonavi.minimap/com.autonavi.map.activity.NewMapActivity

# 关闭App
adb shell am force-stop 包名
# adb shell am force-stop com.autonavi.minimap
```

### 二、frida

#### 1.启动服务

```shell
# 启动frida-server(模拟器-x86架构)
./data/local/tmp/frida-server-12.8.0-android-x86

# 启动frida-server(Pixel真机-arm64架构)
./data/local/tmp/frida-server-14.2.17-android-arm64

# 转发端口
adb forward tcp:27042 tcp:27042
adb forward tcp:27043 tcp:27043

# 启动远超frida-server
./data/local/tmp/frida-server-14.2.17-android-arm64 -l 0.0.0.0:12345
```

#### 2.操作设备

```shell
# 所有连接设备
frida-ls-devices

# 打印USB设备上的所有进程
frida-ps -U
# 打印指定设备上的所有进程
frida-ps -D device_id
# 打印远超设备上的所有进程
frida-ps -H 192.168.137.11:12345

# 杀死USB设备上的指定进程
frida-kill -U PID
# 杀死指定设备上的指定进程
frida-kill -D device_id PID

# 打印设备上的所有应用程序
frida-ps -Ua

# 打印设备上的所有应用程序和对应的包名
frida-ps -Uai
```

### 三、objection

```shell
# 将objection注入应用(Attach)
objection -g com.babytree.apps.pregnancy explore

# 将objection注入远程应用(Attach)
objection -N -h 192.168.137.11 -p 12345 -g com.babytree.apps.pregnancy explore

# 将objection注入应用(Spawn),引号中的objection命令会在启动时就注入App
objection -g com.babytree.apps.pregnancy explore --startup-command "android hooking watch class_method com.babytree.business.common.c.b.a --dump-args --dump-backtrace --dump-return"

# 当前Hook任务
jobs list

# 关闭Hook任务
jobs kill job_id

# 查看内存中加载的so
memory list modules

# 查看so的导出函数
memory list exports libssl.so

# 将so的导出函数保存到json文件中
memory list exports libart.so --json libart.json

# 列出内存中所有的类
android hooking list classes

# 列出类中所有的方法
android hooking list class_methods com.babytree.business.common.c.b

# hook类
android hooking watch class com.babytree.business.common.c.b

# 生成hook类代码
android hooking generate simple com.babytree.business.common.c.b

# hook类的所有重载
android hooking watch class_method com.babytree.business.common.c.b.$init --dump-args --dump-backtrace --dump-return

# hook方法，打印参数、调用栈、返回值
android hooking watch class_method com.babytree.business.common.c.b.a --dump-args --dump-backtrace --dump-return

# 内存堆搜索类的实例
android heap search instances com.babytree.business.common.c.b

# 调用实例的方法
android heap execute 0x2526 a

# 在内存中所有已加载的类中搜索包含特定关键词的类
android hooking search classes 关键词

# 在内存中所有已加载的类的方法中搜索包含特定关键词的方法
android hooking search methods 关键词

# 查看当前可用的activity
android hooking list activities

# 启动activity
android intent launch_activity com.autonavi.map.activity.NewMapActivity
```

### 四、抓包

[r0capture](https://github.com/r0ysue/r0capture)

1. 启动frida-server
2. 打开APP
3. 输入命令

```shell
# Attach
python D:\Projects\learn_forever\Spider\r0capture\r0capture.py -U com.qiyi.video -v -p iqiyi.pcap
# Spawn
python D:\Projects\learn_forever\Spider\r0capture\r0capture.py -U -f com.qiyi.video -v -p iqiyi.pcap
```

### 五、脱壳

[frida-dexdump](https://github.com/hluwa/frida-dexdump)

1. 启动frida-server
2. 打开APP
3. 输入命令

```shell
frida-dexdump
```
