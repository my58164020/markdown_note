# Android逆向环境搭建

## 导读

> Android手机刷机、root、逆向环境搭建，以Google Pixel手机的Android 8.1系统为例。

### 一、基本知识

1. Bootloader锁、Recovery、Fastboot、高通9008模式、OTA、System分区、A/B分区、全盘加密/加密分区等
2. ADB、SuperSU(Magisk)、Xposed(EdXposed，LSPposed)、Frida、Unidbg(Unicorn)等

### 二、准备工作

1. [下载Google USB驱动程序](https://developer.android.com/studio/run/win-usb)

2. [下载Google 官方镜像文件](https://developers.google.com/android/images)

3. [下载SDK Platform Tools](https://developer.android.google.cn/studio/releases/platform-tools?hl=zh_cn)

4. [下载TWRP](https://twrp.me/Devices/)

5. [下载Magisk](https://github.com/topjohnwu/Magisk/releases)

6. [下载Riru](https://github.com/RikkaApps/Riru/releases)

7. [下载EdXposed](https://github.com/ElderDrivers/EdXposed/releases)

8. [下载EdXposedManager](https://github.com/ElderDrivers/EdXposedManager/releases)

9. [下载Frida](https://github.com/frida/frida/releases)

10. 备份手机资料，退出谷歌账号，关闭指纹识别以及锁屏密码

### 三、环境搭建

#### 1.连接手机

1. 手机开启开发者选项，打开USB调试模式

2. 电脑安装Google USB驱动程序

3. 电脑将adb添加到环境变量中

4. 解BL锁

```shell
# 重启进入bootloader
adb reboot bootloader

# 查看连接情况
fastboot devices

# 解锁手机
fastboot oem unlock

# 检查是否已经解锁
fastboot oem device-info
# Device unlocked: true 则表示已经解锁。

# 重启手机
fastboot reboot
```

#### 2.刷入系统

1. 解压镜像文件，找到flash-all.bat文件并运行

2. 大概3-5分钟，刷机就成功了，完成开机设置，开启开发者选项，打开USB调试模式

#### 3.刷入TWRP(Android10及以上跳过)

```shell
# 重启进入bootloader
adb reboot bootloader

# 查看连接情况
fastboot devices

# 临时刷入
fastboot boot twrp-3.3.0-0-sailfish.img

# 临时刷入，A/B分区设备
fastboot flash boot twrp-3.3.0-0-sailfish.img
# A/B分区设备永久刷入需要在twrp中安装twrp-pixel-installer-sailfish-3.3.0-0.zip

# 永久刷入，非A/B分区设备
fastboot flash recovery twrp-3.3.0-0-sailfish.img

# 重启进入Recovery，出现安全警告时直接拖动滑块允许修改
fastboot reboot recovery
```

#### 4.刷入Magisk

##### 1.使用说明

- 从V22版本以后三合一，管理器APP、安装包、卸载包
- Magisk-v22.0.apk，管理器APP
- Magisk-v22.0.zip，安装包
- uninstall.zip，卸载包

##### 2.安装

```shell
# 将下载好的Magisk-v23.0.zip传到手机的sdcard目录中
adb push Magisk-v23.0.zip /sdcard
# 进入TWRP的安装(install)页面，选中刚刚准备好的Magisk的包，刷入它，然后重启
```

##### 3.Android10及以上安装方法

1. 获取boot.img

- Google的官方镜像可以直接解压获取boot.img
- 一加手机[获取方法](https://www.daxiaamu.com/5795/)

2. 安装Magisk Manager Apk(Magisk-v22.0.apk)
3. 将boot.img文件传到手机中，在手机上打开Magisk，选择安装-修补一个文件-选择boot.img文件，打完补丁后是Download目录里magisk_patched.img文件
4. 将magisk_patched.img文件传到电脑中

```shell
# 进入fastboot模式
adb reboot fastboot

# 刷入magisk_patched.img
fastboot flash boot magisk_patched.img
```

#### 5.刷入EdXposed

1. 安装Magisk模块：Riru（作者：Rikka）

2. 安装Magisk模块：EdXposed（作者：SandHook）

3. 安装EdXposedManager

#### 6.刷入Frida

- 安装步骤

```shell
# 推送文件
adb push frida-server-14.2.17-android-arm64 /data/local/tmp

# 修改权限
chmod 777 /data/local/tmp/frida-server-14.2.17-android-arm64

# 开启服务
./data/local/tmp/frida-server-14.2.17-android-arm64

# 验证服务
frida-ps -U
```

- 版本推荐

```shell
# Python3.7 Android8.1
pip install frida==12.8.0
pip install frida-tools==5.3.0
pip install objection==1.8.4

# Python3.8 Android10
pip install frida==14.2.17
pip install frida-tools==9.2.4
pip install objection==1.11.0
```

### 四、其他

#### 1.开机自启WiFi-ADB

- build.prop位于手机的/system/build.prop中，build.prop记录一些系统设置，是一个属性文件，相当于Windows系统的注册表

- build.prop文件的权限为644

- 末尾添加

```shell
# WiFi-ADB
service.adb.tcp.port=5555
persist.service.adb.enable=1
```

- 修改权限

```shell
chmod 644 /system/build.prop
```

#### 2.WiFi图标除去叉号

```shell
adb shell "settings put global captive_portal_https_url https://captive.v2ex.co/generate_204"
```
