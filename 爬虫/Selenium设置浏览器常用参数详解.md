# Selenium设置浏览器常用参数详解

## 导读

> 参数是在定义driver的时候设置，是一个Options类所实例化的对象。参数是设置浏览器是否可视化（加快代码运行速度）和浏览器的请求头（防止网站的反爬虫检测）等信息。

### 一、Options和ChromeOptions

两种方法都可以，指向的源代码都一样。

```python
from selenium.webdriver import ChromeOptions
from selenium.webdriver.chrome.options import Options

options1 = Options()
options2 = ChromeOptions()

```

### 二、常用方法详解

```python
from selenium.webdriver.chrome.options import Options

# 实例化
options = Options()
# 添加启动参数
options.add_argument("")
# 添加扩展应用
options.add_extension("")
options.add_encoded_extension("")
# 添加实验性质的设置参数
options.add_experimental_option("", "")
# 设置调试器地址
options.debugger_address("")

```

### 三、常用参数详解

```python
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

# 实例化
options = Options()
# 无界面
options.add_argument("--headless")
# 无痕模式
options.add_argument("--incognito")
# 以最高权限运行
options.add_argument("--no-sandbox")
# 禁止硬件加速
options.add_argument("--disable-gpu")
# 设置语言
options.add_argument("lang=zh-CN")
# 设置最大化窗口
options.add_argument("--start-maximized")
# 禁止显示"Chrome 正受到自动测试软件的控制。"
options.add_argument("--disable-infobars")
# 禁止拓展
options.add_argument("--disable-extensions")
# 禁止JavaScript
options.add_argument("--disable-javascript")
# 设置代理
options.add_argument("--proxy-server=proxy")
# 设置请求头
options.add_argument("User-Agent=user_agent")
# 添加crx插件
options.add_extension("d:\crx\AdBlock_v2.17.crx")
# 禁止图片
options.add_argument("blink-settings=imagesEnabled=false")
# 禁止显示"Chrome 正受到自动测试软件的控制。"
options.add_experimental_option("useAutomationExtension", False)
# 设置开发者模式启动
options.add_experimental_option("excludeSwitches", ["enable-automation"])
# 设置忽略ssl错误,任何ssl协议
options.add_argument("service_args=['–ignore-ssl-errors=true', '–ssl-protocol=any']")
# 禁止弹窗
options.add_experimental_option(
        "prefs", {"profile.default_content_setting_values": {"notifications": 2}}
)
# 设置用户目录
options.add_argument(
        r"--user-data-dir=C:\Users\Administrator\AppData\Local\Google\Chrome\User Data"
)
# 设置浏览器位置
options.binary_location = "browser_path"
# 启动浏览器
browser = webdriver.Chrome(executable_path="driver_path", chrome_options=options)

```
