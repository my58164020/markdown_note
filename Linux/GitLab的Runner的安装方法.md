# GitLab的Runner的安装方法

## 导读

> Runner用于接收和执行GitLab的CI/CD作业的进程。

### 1、安装

```shell
# 下载可执行文件
curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# 添加可执行权限
chmod 777 /usr/local/bin/gitlab-runner

# 添加软链
ln -snf /usr/local/bin/gitlab-runner /usr/bin/gitlab-runner

# 用root权限运行，避免权限问题
gitlab-runner install --user=root --working-directory=/srv/runner

# 启动服务
gitlab-runner start

#注册runner
gitlab-runner register --url https://gitlab.com/ --registration-token 注册令牌

```

### 2、卸载

```shell
# 停止服务
gitlab-runner stop

# 取消开机启动
chkconfig gitlab-runner off

# 卸载服务
gitlab-runner uninstall

# 清理文件
rm -rf /etc/gitlab-runner
rm -rf /usr/local/bin/gitlab-runner
rm -rf /usr/bin/gitlab-runner
rm -rf /etc/sudoers.d/gitlab-runner

```
