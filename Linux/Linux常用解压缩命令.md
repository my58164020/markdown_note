# Linux常用解压缩命令

## 导读

> 常用的Linux下各种格式的解压缩方法。

### 一、 gzip/gunzip

#### 1.特点

1. 只能压缩文件不能压缩目录
2. 不保留原来的文件
3. 同时压缩多个文件会产生多个压缩包

#### 2.操作

```shell
# 压缩
gzip test.txt

# 解压
gunzip test.txt.gz
```

### 二、zip/unzip

#### 1.特点

1. 压缩命令在Windows/Linux都通用
2. 可以压缩目录且保留源文件

#### 2.操作

```shell
# 压缩
zip test.zip test1.txt test2.txt

# 解压
unzip test.zip

# 解压到指定目录
unzip test.zip -d /home
```

### 三、tar

#### 1.选项

| 选项  | 功能         |
|-----|------------|
| -c  | 产生.tar打包文件 |
| -v  | 显示详细信息     |
| -f  | 指定压缩后的文件名  |
| -z  | 打包同时压缩     |
| -x  | 解包.tar文件   |
| -C  | 解压到指定目录    |

#### 2.操作

```shell
# 压缩文件
tar -zcvf test.tar.gz test1.txt test2.txt

# 压缩目录
tar -zcvf test.tar.gz test/

# 解压
tar -zxvf test.tar.gz

# 解压到指定目录
tar -zxvf test.tar.gz -C /home
```
