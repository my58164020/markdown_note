# Linux查看服务器配置

## 导读

> 常用的Linux下查看服务器配置的命令。

### 一、服务器型号

```shell
# Alibaba Cloud 阿里云
# Tencent Cloud 腾讯云
# Amazon EC2 亚马逊云
dmidecode|grep "System Information" -A9|egrep "Manufacturer|Product"
```

### 二、操作系统

```shell
# 当前操作系统发行版信息
cat /etc/redhat-release
# 操作系统发行版详细信息
sudo yum install -y redhat-lsb
lsb_release -a
```

### 三、CPU

```shell
# CPU统计信息
lscpu
# CPU型号
cat /proc/cpuinfo | grep name | cut -f2 -d: | uniq -c
# 物理CPU个数
cat /proc/cpuinfo| grep "physical id"| sort| uniq| wc -l
# 每个物理CPU中core的个数(即核数)
cat /proc/cpuinfo| grep "cpu cores"| uniq
# 逻辑CPU的个数
cat /proc/cpuinfo| grep "processor"| wc -l
```

### 四、内存

```shell
# 概要内存使用情况[-g是以GB为单位；也可以使用-m，即以MB为单位]
# total:总计物理内存的大小
# used:已使用多大
# free:可用有多少
# Shared:多个进程共享的内存总额
# Buffers/cached:磁盘缓存的大小
free -g
# 内存硬件信息
dmidecode -t memory
# 内存详细使用情况
cat /proc/meminfo
# 内存的插槽数,已经使用多少插槽，每条内存多大
dmidecode|grep -A5 "Memory Device"|grep Size|grep -v Range
```

### 五、硬盘

```shell
# 硬盘和分区分布
# NAME:这是块设备名
# MAJ:MIN:本栏显示主要和次要设备号
# RM:本栏显示设备是否可移动设备
# SIZE:本栏列出设备的容量大小信息
# RO:该项表明设备是否为只读
# TYPE:本栏显示块设备是否是磁盘或磁盘上的一个分区
# MOUNTPOINT:本栏指出设备挂载的挂载点
lsblk
# 硬盘和分区的详细信息
fdisk -l
```

### 六、其他

```shell
# 网卡的硬件信息
lspci | grep -i 'eth'
# 显卡运行状况
nvidia-smi
```
