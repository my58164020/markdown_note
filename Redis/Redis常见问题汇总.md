# Redis常见问题汇总

## 导读

> 使用Redis过程中遇到过一些问题，整理了以下内容，含解决办法。

### 1、MISCONF Redis is configured to save RDB snapshots, but is currently not able to persist on

- 出现原因

Redis被配置为保存数据库快照，但它目前不能持久化到硬盘

猜测是硬盘空间不足，Redis不能保存快照，只能关闭快照或者加硬盘空间

- 解决方法

```shell
# 临时解决
redis-cli
127.0.0.1:6379> config set stop-writes-on-bgsave-error no

# 永久解决
sudo su
sysctl vm.overcommit_memory=1
```
