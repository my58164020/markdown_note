# Redis可视化分析资源占用情况

## 导读

> 使用一个叫做RDR的工具可视化分析Redis的资源占用情况。

1. [RDR下载地址](https://github.com/xueqiu/rdr/releases)

```shell
# 赋予可执行权限
chmod 777 rdr-linux
```

2. 找到dump.rdb文件，Redis默认开启了RDB方式的持久化储存。默认情况下，dump.rdb文件在/var/lib/redis/dump.rdb，只有root用户可以读取。

3. 提权后把dump.rdb文件复制出来，修改成当前用户可操作。

```shell
sudo su
cd ~
cp /var/lib/redis/dump.rdb ./
chown ec2-user:ec2-user dump.rdb
```

4. 获取dump.rdb文件后，让RDR读取dump.rdb文件。

```shell
./rdr-linux show -p 8766 dump.rdb
```

5. 打开浏览器，输入IP:8766，即可可视化分析Redis了。
